<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/supportdent_works.png" alt="Supportdent" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Supportdent"
    eng="Supportdent"
    cat="Supportdent"
></h3>
<span class="subtitulo traduccion" >Ancar Dental</span>
<p class="traduccion "
    esp="Diseño de un brazo mecánico con bandeja de abastecimiento para uso odontológico. Capaz de proporcionar una superficie de apoyo para bandejas, instrumental y accesorios médicos individuales, asegurando la horizontalidad y una compensación constante máxima de 5Kg."
    eng="Design of a mechanical arm with supply tray for dental use. Capable of providing a support surface for individual trays, instruments and medical accessories, ensuring horizontality and a maximum constant offset of 5Kg."
    cat="Disseny d'un braç mecànic amb safata de proveïment per ús odontològic. Capaç de proporcionar una superfície de suport per safates, instrumental i accessoris mèdics individuals, assegurant la horitzontalitat i una compensació constant màxima de 5Kg."
></p>
<p class="traduccion "
    esp="Su diseño se basa en la combinación de líneas elegantes, una forma compacta y una mayor funcionalidad, mejorando su adherencia y movilidad mediante el estudio y optimización de componentes, respecto modelos anteriores."
    eng="Its design is based on the combination of elegant lines, a compact shape and greater functionality, improving its grip and mobility through the study and optimisation of components, compared to previous models."
    cat="El seu disseny es basa en la combinació de línies elegants, una forma compacta i una major funcionalitat, millorant la seva adherència i mobilitat mitjançant l'estudi i optimització de components, respecte models anteriors."
>

</p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Designed for its adaptability and incorporation into the different equipment and workstations developed by Ancar Dental."
    eng="Ideado por su adaptabilidad e incorporación en los diferentes equipos y estaciones de trabajo desarrollados por Ancar Dental."
    cat="Ideat per la seva adaptabilitat i incorporació en els diferents equips i estacions de treball desenvolupats per Ancar Dental."
></p>
<div class="img_diapositivas" pos="0" max="3">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/supportdent (1).jpg" alt="Supportdent">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/supportdent (2).jpg" alt="Supportdent">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/supportdent (3).jpg" alt="Supportdent">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/supportdent (4).jpg" alt="Supportdent">
    <div class="selector"></div>
</div>
