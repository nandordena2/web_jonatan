<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being_works.png" alt="Hidro Well-Being" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Hidro Well-Being"
    eng="Hidro Well-Being"
    cat="Hidro Well-Being"
></h3>
<span class="subtitulo traduccion" class="traduccion"
    esp="Ayuda técnica para plato de ducha"
    eng="Technical support for shower trays"
    cat="Ajuda tècnica per plat de dutxa">
</span>
<p class="traduccion"
    esp="Hydro well-being es una ayuda técnica adaptada para plato de ducha que va dirigida a personas de mediana o avanzada edad con problemas de movilidad."
    eng="Hydro well-being is an adapted technical aid for shower trays for middle-aged and elderly people with mobility problems."
    cat="Hydro well-being és una ajuda tècnica adaptada per a plat de dutxa que va dirigida a persones de mitjana o avançada edat amb problemes de mobilitat."
></p>
<p class="traduccion"
    esp="Confort, higiene, maniobrabilidad y adaptación al entorno son los rasgos identificativos de Hydro well-being."
    eng="Comfort, hygiene, manoeuvrability and adaptation to the environment are the identifying features of Hydro well-being."
    cat="Confort, higiene, maniobrabilitat i adaptació a l'entorn són els trets identificatius d'Hydro well-being."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Aprovecha los múltiples beneficios de la hidroterapia como mejora de la calidad de vida, ofreciendo un diseño compacto, de líneas orgánicas y de fácil manejo tanto para el asistente como la persona discapacitada."
    eng="It takes advantage of the multiple benefits of hydrotherapy as a way of improving quality of life, offering a compact design, with organic lines and easy handling for both the assistant and the disabled person."
    cat="Aprofita els múltiples beneficis de la hidroteràpia com a millora de la qualitat de vida, oferint un disseny compacte, de línies orgàniques i de fàcil maneig tant per l'assistent com la persona discapacitada."
></p>
<div class="img_diapositivas" pos="0" max="4">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being (5).jpg" alt="Hidro Well-Being">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being (1).jpg" alt="Hidro Well-Being">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being (2).jpg" alt="Hidro Well-Being">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being (3).jpg" alt="Hidro Well-Being">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/hidro-well-being (4).jpg" alt="Hidro Well-Being">
    <div class="selector"></div>
</div>
