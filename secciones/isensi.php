<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi_works.png" alt="isensi" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="ISENSI"
    eng="ISENSI"
    cat="ISENSI"
></h3>
<span class="subtitulo traduccion" >ISENSI</span>
<p class="traduccion"
    esp="Isensi es una cabina de estímulos sensoriales. Mediante el efecto hidromasaje con agua pulverizada, ayuda a los cuidadores y familiares en las rutinas de higiene, ofreciendo disfrutar de la sensación de placer durante el baño."
    eng="Isensi is a sensory stimulation cabin. By means of the hydromassage effect with sprayed water, it helps carers and relatives in hygiene routines, offering the sensation of pleasure during bathing."
    cat="Isensi és una cabina d’estímuls sensorials. Mitjançant l’efecte hidromassatge amb aigua polvoritzada, ajuda als cuidadors i familiars en les rutines d’higiene, oferint gaudir de la sensació de plaer durant el bany."
></p>
<p class="traduccion"
    esp="Isensi nace de la necesidad y el confort. Ofrece un diseño rompedor y vanguardista, adaptándose a las necesidades de sus usuarios. Es el producto resultante de integrar el cuidado y la higiene de personas mayores con movilidad reducida, convirtiendo todo el proceso de baño en una experiencia de bienestar."
    eng="Isensi was born out of necessity and comfort. It offers a groundbreaking and avant-garde design, adapting to the needs of its users. It is the product resulting from integrating the care and hygiene of elderly people with reduced mobility, turning the whole bathing process into a wellness experience."
    cat="Isensi neix de la necessitat i el confort. Ofereix un disseny trencador i avantguardista, adaptant-se a les necessitats dels seus usuaris. És el producte resultant d'integrar la cura i la higiene de persones grans amb mobilitat reduïda, convertint tot el procés de bany en una experiència de benestar."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Es una propuesta única en el mercado, que abre una nueva categoría de producto. Actualmente no tiene competencia, aparte de las técnicas de higiene manual y los equipos de ducha y baño tradicionales."
    eng="It is a unique proposition on the market, opening up a new product category. It currently has no competition, apart from manual hygiene techniques and traditional shower and bathing equipment."
    cat="És una proposta única en el mercat, que obre una nova categoria de producte. Actualment no té competència, apart de les tècniques d’higiene manual i els equips de dutxa i bany tradicionals."
></p>
<div class="img_diapositivas" pos="0" max="5">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (1).jpg" alt="isensi">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (2).jpg" alt="isensi">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (3).jpg" alt="isensi">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (4).jpg" alt="isensi">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (5).jpg" alt="isensi">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi (6).jpg" alt="isensi">
    <div class="selector"></div>
</div>
