<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/Marquesina BMDD_works.png" alt="Marquesina BMDD" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Marquesina BMDD"
    eng="Marquee BMDD"
    cat="Marquesina BMDD"
></h3>
<span class="subtitulo traduccion" >Trinity Industries </span>
<p class="traduccion"
    esp="Diseño y desarrollo de una parada de bus modular con el objetivo de habilitar el acceso peatonal garantizando la máxima seguridad y movilidad urbana de los usuarios del transporte público, así como mejorar el servicio y la funcionalidad del punto de parada y de su entorno."
    eng="Design and development of a modular bus stop with the aim of enabling pedestrian access, guaranteeing maximum safety and urban mobility for public transport users, as well as improving the service and functionality of the stop and its surroundings."
    cat="Disseny i desenvolupament d'una parada de bus modular amb l'objectiu d'habilitar l'accés als vianants garantint la màxima seguretat i mobilitat urbana dels usuaris del transport públic, així com millorar el servei i la funcionalitat del punt de parada i del seu entorn."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="La estandarización y la funcionalidad aplicada a la integración de funciones, permite simplificar las instalaciones en la vía pública, facilitando una parada de bus flexible, organizando los servicios y las instalaciones areas"
    eng="The standardisation and functionality applied to the integration of functions allows for simplified on-street installations, facilitating a flexible bus stop, organising services and overhead facilities."
    cat="L'estandardització i la funcionalitat aplicada a la integració de funcions, permet simplificar les instal·lacions a la via pública, facilitant una parada de bus flexible, organitzant els serveis i les instal·lacions aries."
></p>
<div class="img_diapositivas" pos="0" max="3">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/Marquesina BMDD 2.jpg" alt="Marquesina BMDD">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/Marquesina BMDD 3.jpg" alt="Marquesina BMDD">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/Marquesina BMDD 4.jpg" alt="Marquesina BMDD">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/Marquesina BMDD 5.jpg" alt="Marquesina BMDD">
    <div class="selector"></div>
</div>
