<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/kangaroo_works.png" alt="kangaroo" class="imagen_slide">
</div>
<h3 class="traduccion left">kangaroo</h3>
<span class="subtitulo traduccion" class="traduccion"
    esp="Silla de ruedas bipdestadora"
    eng="Bipodestator wheelchair"
    cat="Cadira de rodes bipdestadora"
></span>
<p class="traduccion "
    esp="Diseño de una silla de ruedas para uso urbano adaptada a las tendencias y necesidades actuales. Creada para solucionar las barreras arquitectónicas y estructurales existentes, tanto en la ciudad como en las que repercuten directamente con esta tipología de productos."
    eng="Design of a wheelchair for urban use adapted to current trends and needs. Created to solve existing architectural and structural barriers, both in the city and in those that have a direct impact on this type of product."
    cat="Disseny d'una cadira de rodes per a ús urbà adaptada a les tendències i necessitats actuals. Creada per solucionar les barreres arquitectòniques i estructurals existents, tant a la ciutat com en les que repercuteixen directament amb aquesta tipologia de productes."
></p>
<p class="traduccion "
    esp="Isla ofrece un producto versátil y de gran maniobrabilidad con una estética joven y asequible."
    eng="Isla offers a versatile and highly manoeuvrable product with a young and affordable aesthetic."
    cat="Kangaroo ofereix un producte versàtil i de gran maniobrabilitat amb una estètica jove i assequible."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Se crea un concepto de fácil mantenimiento, con un gran carácter y amplias posibilidades de movilidad para facilitar el día a día de sus usuarios, mediante la combinación de un chasis inspirado en las bicis de montaña, la aplicación de componentes estándar de mercado y el desarrollo de un sistema neumático."
    eng="It creates a concept of easy maintenance, with a great character and wide mobility possibilities to facilitate the daily life of its users, through the combination of a chassis inspired by mountain bikes, the application of standard market components and the development of a pneumatic system."
    cat="Es crea un concepte de fàcil manteniment, amb un gran caràcter i àmplies possibilitats de mobilitat per facilitar el dia a dia dels seus usuaris, mitjançant la combinació d'un xassís inspirat en les bicis de muntanya, l'aplicació de components estàndard de mercat i el desenvolupament d'un sistema pneumàtic."
></p>
<div class="img_diapositivas" pos="0" max="3">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/kangaroo (1).jpg" alt="texto alt">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/kangaroo (2).jpg" alt="texto alt">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/kangaroo (3).jpg" alt="texto alt">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/kangaroo (4).jpg" alt="texto alt">
    <div class="selector"></div>
</div>
