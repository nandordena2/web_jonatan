<?
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
$DOCUMENT_HTTP = "http".(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!="off"?"s":"")."://".$_SERVER["SERVER_NAME"];

$imagenes = scandir($DOCUMENT_ROOT."/img/");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>IMG</title>
        <style media="screen">
            .imagenes{
                display: flex;
                flex-wrap: wrap;
            }
            a{
                display: block;
                cursor: pointer;
                margin: 5px;
            }
            a:hover{
                filter: brightness(1.5);
            }
            img{
                height: 30px;
                padding: 5px;
                background: #888;
            }
        </style>
    </head>
    <body>
        <h1>Imagenes disponibles</h1>
        <div class="imagenes">
            <?
            foreach ($imagenes as $key => $value) {
                if(
                    $value != "."
                    && $value != ".."
                    && $value != ""
                    && $value != "index.php"
                    && $value != "test.path"

                ){
                    echo '<a href="'.$DOCUMENT_HTTP.'/img/'.$value.'">';
                    echo '<img src="'.$value.'">';
                    echo '</a>';
                }
            }
            ?>
        </div>
    </body>
</html>
