<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/secciones/contacto.css">
<div class="contenedor-imagen-adaptativa">
    <img src="/img/contacto.jpg" alt="contacto">
</div>
<section class="contacto">
    <h3> Studio<span class="blanco">Liquid</span> </h3>
    <div class="ancho_maximo">
        <p class="traduccion"
            esp="Contacta’ns"
            eng=""
            cat=""
        ></p>
        <p>
            <br><a href="mailto:contact@studio-liquid.com">contact@studio-liquid.com</a>
            <br><a href="tel:+34 626 88 76 75">+34 626 88 76 75</a>
            <a href="tel:+34 639 08 02 84">+34 639 08 02 84</a>
        </p>
        <p>
            Girona
            <br>
            <a target="_blank" href="https://www.google.com/maps/place/Parc+Científic+i+Tecnològic+de+la+Universitat+de+Girona/@41.9673844,2.8371452,15z/data=!4m5!3m4!1s0x0:0x3084901b32f4cc28!8m2!3d41.9673844!4d2.8371452">
                <br>OT central
                <br>Parc Científic i Tecnològic de la Universitat de Girona
                <br>Pic de Peguera, 17003, Girona
                <br>N41.967000, E2.837000
            </a>
        </p>
        <p>
            Olot
            <br>
            <a target="_blank" href="https://www.google.com/maps/place/42°10'52.7N+2°29'28.2E">
                <br>Bisbe Lorenzana, 17800, Olot
                <br>N42.181311, E2.491170
            </a>
        </p>
        <p>
            Vic
            <br>
            <a target="_blank" href="https://goo.gl/maps/jG6kMzGS5E1arxqm8">
                <br>Sant Jordi, 19, 08500, Vic
                <br>N41.931214, E2.262047
            </a>
        </p>
    </div>
</section>
