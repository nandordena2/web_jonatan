window.addEventListener("load",()=>{
    scroll_galeria();
})
function scroll_galeria() {
    Object.values( document.querySelectorAll(".scroll_galeria") )
    .forEach((ele) => {
        movimiento_scroll_galeria(ele);
        // autoscroll_galeria(ele);
        ele.setAttribute("over",0);
        ele.setAttribute("direccion",1);
        ele.addEventListener("mousemove",()=>{evento_hover_scroll_galeria(ele);});
        ele.addEventListener("mouseout",()=>{
            ele.setAttribute("over",0);
        });
    });
}
function movimiento_scroll_galeria(ele){
    if(ele.getAttribute("over")=="1"){
    }
    else if(ele.getAttribute("direccion")==1){
        if(ele.scrollWidth - ele.offsetWidth == ele.scrollLeft){
            ele.setAttribute("direccion",0);
        }else{
            ele.scrollTo(ele.scrollLeft+2,0)
        }
    }
    else{
        if(0 == ele.scrollLeft){
            ele.setAttribute("direccion",1);
        }else{
            ele.scrollTo(ele.scrollLeft-2,0)
        }
    }
    setTimeout(()=>{movimiento_scroll_galeria(ele);},50);
}
function evento_hover_scroll_galeria(ele){
    let margen = 200;

    let client = event.clientX - ele.offsetLeft;
    if(client>ele.offsetWidth)client = ele.offsetWidth;

    ele.setAttribute("over",1);
    proporcion = ((ele.scrollWidth - ele.offsetWidth ) * (
        client
        /(ele.offsetWidth-margen*1.5)
    ))-margen;
    ele.scrollTo(proporcion,0);
}
function menu_idioma(idioma){
    let idioma_ele = document.querySelector("menu .idioma");
    idioma_ele.setAttribute("desplegado",idioma_ele.getAttribute("desplegado")==1?0:1);
    if(document.body.getAttribute("idioma")!=idioma){
        window.location.search="?l="+idioma;
    }
}
