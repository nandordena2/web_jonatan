<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/secciones/slider.css">
<div class="slider img_diapositivas2" pos="1">
    <div class="slide">
        <div class="texto">
            <div class="fondo_blanco">
                <h2 class="sub traduccion"
                    esp="Reduzca riesgos y complicaciones"
                    eng="Reduce risks and complications"
                    cat="Redueixi riscos i complicacions"
                ></h2>
                <p>
                    <span class="traduccion"
                        esp="Studio Liquid junto a su empresa para dar"
                        eng="Studio Liquid together with your company to give"
                        cat="Studio Liquid al costat de la seva empresa per donar"
                    ></span>.
                    <span class="traduccion marker_green"
                        esp="apoyo y asesoramiento"
                        eng="support and advice"
                        cat="suport i assesorament"
                    ></span>,
                    <span class="traduccion"
                        esp="tanto en el departamento de ingeniería como en el desarrollo de nuevos retos"
                        eng="as well as in the development of new challenges"
                        cat="tant al departament d’enginyeria com en el desenvolupament de nous reptes"
                    ></span>.
                </p>
            </div>
        </div>
        <img src="<?php echo $DOCUMENT_HTTP?>/img/estructura_slider.jpg" alt="isensi">
    </div>
    <div class="slide">
        <div class="texto">
            <div class="fondo_blanco fuerte">
                <h2 class="sub traduccion"
                    esp="Reduzca riesgos y complicaciones"
                    eng="Reduce risks and complications"
                    cat="Redueixi riscos i complicacions"
                ></h2>
                <p>
                    <span class="traduccion"
                        esp="Studio Liquid junto a su empresa para dar"
                        eng="Studio Liquid together with your company to give"
                        cat="Studio Liquid al costat de la seva empresa per donar"
                    ></span>.
                    <span class="traduccion marker_green"
                        esp="apoyo y asesoramiento"
                        eng="support and advice"
                        cat="suport i assesorament"
                    ></span>,
                    <span class="traduccion"
                        esp="tanto en el departamento de ingeniería como en el desarrollo de nuevos retos"
                        eng="as well as in the development of new challenges"
                        cat="tant al departament d’enginyeria com en el desenvolupament de nous reptes"
                    ></span>.
                </p>
            </div>
        </div>
        <img src="<?php echo $DOCUMENT_HTTP?>/img/taller_slider.jpg" alt="isensi">
    </div>
    <div class="slide">
        <div class="texto">
            <div class="fondo_blanco">
                <h2 class="sub traduccion"
                    esp="Reduzca riesgos y complicaciones"
                    eng="Reduce risks and complications"
                    cat="Redueixi riscos i complicacions"
                ></h2>
                <p>
                    <span class="traduccion"
                        esp="Studio Liquid junto a su empresa para dar"
                        eng="Studio Liquid together with your company to give"
                        cat="Studio Liquid al costat de la seva empresa per donar"
                    ></span>.
                    <span class="traduccion marker_green"
                        esp="apoyo y asesoramiento"
                        eng="support and advice"
                        cat="suport i assesorament"
                    ></span>,
                    <span class="traduccion"
                        esp="tanto en el departamento de ingeniería como en el desarrollo de nuevos retos"
                        eng="as well as in the development of new challenges"
                        cat="tant al departament d’enginyeria com en el desenvolupament de nous reptes"
                    ></span>.
                </p>
            </div>
        </div>
        <img src="<?php echo $DOCUMENT_HTTP?>/img/planos_slider.jpg" alt="isensi">
    </div>
    <div class="slide">
        <div class="texto">
            <div class="fondo_blanco">
                <h2 class="sub traduccion"
                    esp="Reduzca riesgos y complicaciones"
                    eng="Reduce risks and complications"
                    cat="Redueixi riscos i complicacions"
                ></h2>
                <p>
                    <span class="traduccion"
                        esp="Studio Liquid junto a su empresa para dar"
                        eng="Studio Liquid together with your company to give"
                        cat="Studio Liquid al costat de la seva empresa per donar"
                    ></span>.
                    <span class="traduccion marker_green"
                        esp="apoyo y asesoramiento"
                        eng="support and advice"
                        cat="suport i assesorament"
                    ></span>,
                    <span class="traduccion"
                        esp="tanto en el departamento de ingeniería como en el desarrollo de nuevos retos"
                        eng="as well as in the development of new challenges"
                        cat="tant al departament d’enginyeria com en el desenvolupament de nous reptes"
                    ></span>.
                </p>
            </div>
        </div>
        <img src="<?php echo $DOCUMENT_HTTP?>/img/isensi_slider.jpg" alt="isensi">
    </div>
    <div class="selector" style="display:none;"></div>
</div>
