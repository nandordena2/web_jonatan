window.addEventListener("load",()=>{
    Object.values(document.querySelectorAll(".img_diapositivas"))
    .concat(Object.values(document.querySelectorAll(".img_diapositivas2")))
    .forEach(ele => {
        setTimeout(()=>{autoscroll_diapositivas(ele);},15*1000);//segundos
        let ele_selector = ele.querySelector(".selector");
        Object.values(ele.querySelectorAll("img, iframe"))
        .forEach((img, i) => {
            let box = document.createElement('div');
            box.className='box';
            box.addEventListener('click',()=>{
                ele.setAttribute("stop",1);
                ele.setAttribute("pos",i);
                if(
                    img.nodeName=="IFRAME"
                    && img.src.search("autoplay") == -1
                ){
                    img.src = img.src.split("?")[0]+"?autoplay=1&loop=1";
                }else{
                    Object.values( ele.querySelectorAll("iframe") )
                    .forEach(i=>{
                        i.src = i.src.split("?")[0];
                    });
                }
            });
            ele_selector.appendChild(box);
        });
    });
});
function autoscroll_diapositivas(ele){
    if(ele.getAttribute("stop")==1)return;//parar desplazamiento;
    let pos = ele.getAttribute("pos");
    let max = (ele.children.length-1) -1;
    if(pos>=max){
        ele.setAttribute("pos",1);
    }else{
        ele.setAttribute("pos",(pos*1)+1);
    }
    setTimeout(()=>{autoscroll_diapositivas(ele);},10*1000);//segundos
}
