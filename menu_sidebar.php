<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/menu_sidebar.css">
<section class="menu_sidebar">
    <!-- <a href="#"> -->
        <div class="boton_imagen siempre">
            <div class="traduccion"
            esp="Ingenieria"
            eng="Engineering"
            cat="Enginyeria"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/INGENIERIA menu.jpg" alt="Ingenieria">
        </div>
    <!-- </a> -->
    <a href="/works/kangaroo">
        <div class="boton_imagen siempre">
            <div class="traduccion"
            esp="Kangaro"
            eng="Kangaro"
            cat="Kangaro"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/KANGAROO menu.jpg" alt="Kangaro">
        </div>
    </a>
    <a href="/works/supportdent">
        <div class="boton_imagen siempre">
            <div class="traduccion"
            esp="Supportdent"
            eng="Supportdent"
            cat="Supportdent"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/SUPOORTDENT_menu.jpg" alt="Supportdent">
        </div>
    </a>
    <a href="/works/magna extruder">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="Magna"
                eng="Magna"
                cat="Magna"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/magna.jpg" alt="Magna">
        </div>
    </a>
    <a href="/works/effimer">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="Effimer"
                eng="Effimer"
                cat="Effimer"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/EFFIMER menu.jpg" alt="Effimer">
        </div>
    </a>
    <a href="/works/gandula-iris">
        <div class="boton_imagen siempre">
            <div class="traduccion"
                esp="Gandula IRIS"
                eng="Gandula IRIS"
                cat="Gandula IRIS"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/GANDULA IRIS menu.jpg" alt="Gandula IRIS">
        </div>
    </a>
    <a href="/works/hidro-well-being">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="Hidro Well-being"
                eng="Hidro Well-being"
                cat="Hidro Well-being"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/Hidro well-being_menu.jpg" alt="Hidro Well-being">
        </div>
    </a>
    <a href="/works/citysafe">
        <div class="boton_imagen siempre">
            <div class="traduccion"
                esp="Citysafe"
                eng="Citysafe"
                cat="Citysafe"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/Citysafe_menu.jpg" alt="Citysafe">
        </div>
    </a>
    <a href="/works/isensi">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="ISENSI"
                eng="ISENSI"
                cat="ISENSI"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/ISENSI_menu.jpg" alt="ISENSI">
        </div>
    </a>
    <a href="/works/marquee">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="Marquesina"
                eng="Marquee"
                cat="Marquesina"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/MARQUESINA BMDD_menu.jpg" alt="Antena">
        </div>
    </a>
    <a href="/works/tlv-series">
        <div class="boton_imagen">
            <div class="traduccion"
                esp="TLV SERIES"
                eng="TLV SERIES"
                cat="TLV SERIES"
            ></div>
            <img src="<?php echo $DOCUMENT_HTTP?>/img/TLV SERIES_menu.jpg" alt="TLV SERIES">
        </div>
    </a>
    <a href="/works" style="width: 100%; margin-bottom: 6px;">
        <div class="boton_imagen solo siempre" style="background:#ab0; height: 80px;">
            <div class="traduccion"
                esp="ver más"
                eng="see more"
                cat="veure més"
            ></div>
        </div>
    </a>
    <div class="selector"></div>
</section>
