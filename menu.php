<?php
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $DOCUMENT_HTTP = "http".(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!="off"?"s":"")."://".$_SERVER["SERVER_NAME"];
?>
<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/menu.css">
<script type="text/javascript">
    function click_menu(ele){
        let lang = document.body.getAttribute("idioma");
        window.location =
            window.location.origin
            +"/"+ele.getAttribute(lang).toLocaleLowerCase()
            +window.location.search;
    }
</script>
<menu class="ancho_maximo">
    <a href="<?php echo $DOCUMENT_HTTP."?".$_SERVER['QUERY_STRING']?>" class="logo">
        <img src="<?php echo $DOCUMENT_HTTP?>/img/liquid.svg" alt="Studio Liquid">
    </a>

    <div class="secciones">
        <a class="seccion traduccion" href="<?php echo $DOCUMENT_HTTP."?".$_SERVER['QUERY_STRING']?>"
            esp="Inicio"
            eng="Home"
            cat="Inici"
        ></a>
        <a class="seccion traduccion" onclick="click_menu(this);"
            esp="Nosotros"
            eng="About"
            cat="Nosaltres"
        ></a>
        <a class="seccion traduccion" onclick="click_menu(this);"
            esp="Trabajos"
            eng="Works"
            cat="Treballs"
        ></a>
        <a class="seccion traduccion" onclick="click_menu(this);"
            esp="Contacto"
            eng="Contact"
            cat="Contacte"
        ></a>
        <div class="seccion idioma">
            <div class="esp" onclick="menu_idioma('esp')">Esp</div>
            <div class="cat" onclick="menu_idioma('cat')">Cat</div>
            <div class="eng" onclick="menu_idioma('eng')">Eng</div>
        </div>
    </div>
</menu>
