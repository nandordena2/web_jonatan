<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/secciones/home.css">
<section class="home">
    <?php include $_SERVER['DOCUMENT_ROOT']."/secciones/slider.php"?>
    <div class="vertical ancho_maximo">
        <div>
            <h3 class="sub traduccion"
                esp="¿Necesita apoyo a la ingeniería?"
                eng="Do you need engineering support?"
                cat="Necesita suport a l’enginyeria?"
            ></h3>
            <p>
                <span class="traduccion"
                    esp="Estudiamos y adaptamos tus necesidades para poder ofrecerte"
                    eng="We study and adapt to your needs in order to be able to offer you"
                    cat="Estudiem i adaptem les teves necessitats per poder oferir-te"
                ></span>
                <span class="marker_green traduccion"
                    esp="apoyo dentro de tu departamento técnico"
                    eng="support within your technical department"
                    cat="recolzament dins del teu departament tècnic"
                ></span>.
                <span class="traduccion"
                    esp="Modernizando las herramientas de trabajo, agilizando los proyectos o ofreciendo nuevas oportunidades de meract"
                    eng="Modernising work tools, streamlining projects or offering new market opportunities"
                    cat="Modernitzant les eines de treball, agilitzant els projectes o oferint noves oportunitats de meract"
                ></span>.
            </p>
        </div>
        <div>
            <h3 class="sub traduccion"
                esp="¿Quiere desarrollar un nuevo proyecto? "
                eng="Do you want to develop a new project?"
                cat="Vol desenvolupar un nou projecte?"
            ></h3>
            <p>
                <span class="traduccion marker_green"
                    esp="Diseñamos y desarrollamos tus proyectos"
                    eng="We design and develop your projects"
                    cat="Dissenyem i desenvolupem els teus projectes"
                ></span>,
                <span class="traduccion"
                    esp="ofreciendo soluciones funcionales y innovadoras adaptadas a los procesos y necesidades de tu empresa y de tus clientes"
                    eng="offering functional and innovative solutions adapted to the processes and needs of your company and your customers"
                    cat="oferint solucions funcionals e innovadores adaptades als processos i necessitats de la teva empresa i dels teus clients"
                ></span>.
            </p>
        </div>
    </div>
    <?php include $DOCUMENT_ROOT."/menu_sidebar.php"; ?>
</section>
