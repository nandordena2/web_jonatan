<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/citysafe_works.png" alt="Citysafe" class="imagen_slide">
</div>
<h3 class="traduccion left"
>Citysafe NY</h3>
<span class="subtitulo traduccion" >Trinity Industries inc</span>
<p class="traduccion"
    esp="Trinity Industries es un proveedor líder en productos ferroviarios. Actualmente también posee una empresa spin-off focalizada en infraestructuras y mobiliario urbano."
    eng="Trinity Industries is a leading supplier of railway products. It also currently owns a spin-off company focused on infrastructure and street furniture."
    cat="Trinity Industries és un proveïdor líder en productes ferroviaris. Actualment també posseeix una empresa spin-off focalitzada en infraestructures i mobiliari urbà."
></p>
<p class="traduccion"
    esp="Se han desarrollado dos líneas de trabajo según las necesidades surgidas dentro del flujo constante de usuarios de la ciudad de New York."
    eng="Two lines of work have been developed according to the needs arising from the constant flow of users in New York City."
    cat="S'han desenvolupat dues línies de treball segons les necessitats sorgides dins el flux constant d'usuaris de la ciutat de New York."
></p>
<p class="traduccion"
    esp="Hedgehog NY, facilita una instalación rápida y segura, ofreciendo una estructura de líneas simples y minimalistas, con acabados de gran calidad para una mayor integración en entornos cosmopolitas."
    eng="Hedgehog NY, facilitates a quick and safe installation, offering a structure of simple and minimalist lines, with high quality finishes for greater integration in cosmopolitan environments."
    cat="Hedgehog NY, facilita una instal·lació ràpida i segura, oferint una estructura de línies simples i minimalistes, amb acabats de gran qualitat per a una major integració en entorns cosmopolites."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Green Hammer NY, se concibe con el fin de introducir pilones de protección K4 y K8, en entornos urbanos, sin que éstos sean percibidos por la población. Diseño con trazos y formas limpias, que proporciona una gran variedad de acabados según la identidad y necesidades de cada ciudad."
    eng="Green Hammer NY, was conceived with the aim of introducing K4 and K8 protection pylons in urban environments, without them being perceived by the population. Design with clean lines and shapes, which provides a wide variety of finishes according to the identity and needs of each city."
    cat="Green Hammer NY, es concep amb la finalitat d'introduir pilones de protecció K4 i K8, en entorns urbans, sense que aquests siguin percebuts per la població. Disseny amb traços i formes netes, que proporciona una gran varietat d'acabats segons la identitat i necessitats de cada ciutat."
></p>
<div class="img_diapositivas" pos="0" max="3">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/citysafe (4).jpg" alt="Citysafe">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/citysafe (1).jpg" alt="Citysafe">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/citysafe (2).jpg" alt="Citysafe">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/citysafe (3).jpg" alt="Citysafe">
    <div class="selector"></div>
</div>
