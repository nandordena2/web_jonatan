<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris_works.png" alt="Glandula iris" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Glandula IRIS"
    eng="Glandula IRIS"
    cat="Glandula IRIS"
></h3>
<span class="subtitulo traduccion" >Balliu Export</span>
<p class="traduccion"
    esp="La colección Iris surge a partir de la optimización y mejora de los procesos de fabricación. Ofrece un diseño estilizado de trazos suaves, aportando serenidad y elegancia al conjunto."
    eng="The Iris collection is the result of the optimisation and improvement of manufacturing processes. It offers a stylised design with soft lines, bringing serenity and elegance to the whole."
    cat="La col·lecció Iris sorgeix a partir de l'optimització i millora dels processos de fabricació. Ofereix un disseny estilitzat de traços suaus, aportant serenitat i elegància al conjunt."
></p>
<p class="traduccion"
    esp="Tanto en cuanto a su estructura, como sus componentes, han sido desarrollados para facilitar su montaje, almacenamiento, transporte y uso de forma eficiente y confortable."
    eng="Both its structure and its components have been developed to facilitate its assembly, storage, transport and use in an efficient and comfortable way."
    cat="Tant pel que fa la seva estructura, com els seus components, han estat desenvolupats per facilitar el seu muntatge, emmagatzematge, transport i ús de forma eficient i confortable."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Desarrollado en perfiles de aluminio endurecido y materiales reciclados para consolidar un producto seguro y duradero, sin que pierda su carácter con el paso el tiempo."
    eng="Developed in hardened aluminium profiles and recycled materials to consolidate a safe and durable product, without losing its character over time."
    cat="Desenvolupat en perfils d'alumini endurit i materials reciclats per consolidar un producte segur i durador, sense que perdi el seu caràcter amb el pas el temps."
></p>
<div class="img_diapositivas" pos="0" max="4">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris (1).jpg" alt="Glandula iris">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris (2).jpg" alt="Glandula iris">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris (3).jpg" alt="Glandula iris">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris (4).jpg" alt="Glandula iris">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/iris (5).jpg" alt="Glandula iris">
    <div class="selector"></div>
</div>
