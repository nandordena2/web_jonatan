<?php
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $DOCUMENT_HTTP = "http".(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!="off"?"s":"")."://".$_SERVER["SERVER_NAME"];
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>SL</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/main.css">
        <link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/traducciones.css">
        <link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/img_diapositivas.css">

        <script type="text/javascript" src="<?php echo $DOCUMENT_HTTP?>/img_diapositivas.js"> </script>
        <script type="text/javascript" src="<?php echo $DOCUMENT_HTTP?>/main.js"> </script>

        <!-- Dispositivo tactil -->
        <script type="text/javascript">
            window.addEventListener("load",()=>{
                if (('ontouchstart' in window) ||
                    (navigator.maxTouchPoints > 0) ||
                    (navigator.msMaxTouchPoints > 0))
                    { document.body.setAttribute("touch",1);
                }else document.body.setAttribute("touch",0);
            });
        </script>

        <!-- CHAT -->
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/605b4cdbf7ce182709337bd8/1f1ia8f5q';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
        
    </head>
    <body
        seccion="<?php echo isset($SECCION)?$SECCION:"inicio"; ?>"
        idioma="<?php
            //IDIOMA DEL CLIENTE
            if(
                isset($_GET['l'])
                && in_array($_GET['l'],["esp","cat","eng"])
            ) $lang = substr($_GET['l'],0,2);
            else $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
            switch ($lang){
                case "es": echo "esp"; break;
                case "ca": echo "cat"; break;
                case "en": echo "eng"; break;
                default: echo "eng"; break;
            }
        ?>"
    >
        <?php include $_SERVER['DOCUMENT_ROOT']."/menu.php";?>
        <section>
            <?php
                // switch (strtolower( explode("/",$_SERVER['REQUEST_URI'])[1])) {
                switch (preg_replace("/\/(.*)\/[^\/]*?.*/","$1",(strtolower($_SERVER['REQUEST_URI'])))) {
                    case 'contacto':
                    case 'contacte':
                    case 'contact':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/contacto.php"; break;
                    case 'trabajos':
                    case 'treballs':
                    case 'works':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/trabajos.php"; break;
                    case 'nosotros':
                    case 'nosaltres':
                    case 'About':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/nosotros.php"; break;
                    case 'privacidad':
                    case 'privacitat':
                    case 'privacity':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/privacidad.php"; break;
                    // TRABAJOS
                    case 'works/ingenieria':
                    case 'works/engineering':
                    case 'works/enginyeria':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/ingenieria.php"; break;
                    case 'works/kangaroo':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/kangaroo.php"; break;
                    case 'works/supportdent':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/supportdent.php"; break;
                    case 'works/extrusora magna':
                    case 'works/extrusora%20magna':
                    case 'works/magna extruder':
                    case 'works/magna%20extruder':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/Extrusora_MAGNA.php"; break;
                    case 'works/effimer':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/effimer.php"; break;
                    case 'works/gandula-iris':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/gandula_iris.php"; break;
                    case 'works/hidro-well-being':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/hidro_well_being.php"; break;
                    case 'works/citysafe':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/citysafe.php"; break;
                    case 'works/isensi':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/isensi.php"; break;
                    case 'works/marquesina':
                    case 'works/marquesina':
                    case 'works/marquee':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/marquesina_bmdd.php"; break;
                    case 'works/tlv-series':
                        include $_SERVER['DOCUMENT_ROOT']."/secciones/tlv_series.php"; break;
                    default: include $_SERVER['DOCUMENT_ROOT']."/secciones/home.php"; break;
                }
            ?>
        </section>
        <?php include $_SERVER['DOCUMENT_ROOT']."/footer.php";?>
    </body>
</html>
