<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img\Extrusora MAGNA_works.png" alt="Marquesina BMDD" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Extrusora MAGNA"
    eng="MAGNA Extruder"
    cat="Extrusora MAGNA"
></h3>
<span class="subtitulo traduccion" >Verdés S.A.</span>
<p class="traduccion"
    esp="La empresa Verdés, con más de 100 años de liderazgo en el sector cerámico, ofrece una amplia gama de productos. Para poder competir en el mercado actual se desarrolló una nueva línea de productos que une tanto la experiencia como el diseño."
    eng="The company Verdés, with more than 100 years of leadership in the ceramic sector, offers a wide range of products. In order to be able to compete in today's market, a new product line was developed that combines both experience and design."
    cat="L'empresa Verdés, amb més de 100 anys de lideratge en el sector ceràmic, ofereix una àmplia gamma de productes. Per poder competir en el mercat actual es va desenvolupar una nova línia de productes que uneix tant l'experiència com el disseny."
></p>
<p class="traduccion"
    esp="La extrusora Magna nace con el objetivo de vincular la tecnología y el diseño en el servicio de la industria. El diseño une la robustez y la simplicidad de la forma, diferenciando las zonas de trabajo, integrando las diferentes partes e incorporando una identidad de marca unificada en sus equipos."
    eng="The Magna extruder was created with the aim of linking technology and design in the service of industry. The design unites robustness and simplicity of form, differentiating the work areas, integrating the different parts and incorporating a unified brand identity in its equipment."
    cat="L'extrusora Magna neix amb l'objectiu de vincular la tecnologia i el disseny a el servei de la indústria. El disseny uneix la robustesa i la simplicitat de la forma, diferenciant les zones de treball, integrant les diferents parts i incorporant una identitat de marca unificada en els seus equips."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Como resultado, un producto competitivo que aporta una reducción del coste de fabricación de un 15% a un 20%."
    eng="As a result, a competitive product that brings a reduction in manufacturing cost of 15% to 20%."
    cat="Com a resultat, un producte competitiu que aporta una reducció del cost de fabricació d'un 15% a un 20%."
></p>
<div class="img_diapositivas" pos="0" max="4">
    <img src="<?php echo $DOCUMENT_HTTP?>/img\Extrusora MAGNA 1.jpg" alt="Marquesina BMDD">
    <img src="<?php echo $DOCUMENT_HTTP?>/img\Extrusora MAGNA 3.jpg" alt="Marquesina BMDD">
    <img src="<?php echo $DOCUMENT_HTTP?>/img\Extrusora MAGNA 4.jpg" alt="Marquesina BMDD">
    <iframe src="https://www.youtube.com/embed/2H50Ur7_Qqw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    <div class="selector"></div>
</div>
