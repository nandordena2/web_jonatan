<div class="contenedor-imagen-adaptativa">
    <img src="/img/privacidad.jpg" alt="privacity" class="imagen_slide">
</div>
<h2 class="traduccion titulo_privacidad"
    esp="Información detallada Protección de Datos"
    eng="Detailed information Data Protection"
    cat="Informació detallada Protecció de Dades"
></h2>
<p class="traduccion"
    esp="Sen cumplimiento con el Reglamento General de Protección de Datos relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales ya la libre circulación de estos datos (RGPD), le facilitamos la siguiente información detallada del tratamiento de datos personales :"
    eng="In compliance with the General Data Protection Regulation on the protection of individuals with regard to the processing of personal data and on the free movement of such data (GDPR), we provide you with the following detailed information on the processing of personal data:"
    cat="Sen compliment amb el Reglament General de Protecció de Dades relatiu a la protecció de les persones físiques en el que respecta al tractament de dades personals i a la lliure circulació d'aquestes dades (RGPD), li facilitem la següent informació detallada del tractament de dades personals:"
></p>
<p>
    <b class="traduccion marker_green"
        esp="1-Responsable del tratamiento:"
        eng="1-Treatment manager:"
        cat="1-Responsable del tractament:"
    ></b>
    <span class="traduccion"
        esp="Studio Liquid, propietario de este dominio web, acepta respetar la legislación vigente sobre protección y confidencialidad en el tratamiento de datos personales."
        eng="Studio Liquid, owner of this web domain, agrees to respect the current legislation on protection and confidentiality in the processing of personal data."
        cat="Studio Liquid, propietari d’aquest domini web, accepta respectar la legislació vigent sobre protecció i confidencialitat en el tractament de dades personals."
    ></span>
</p>
<p>
    <b class="traduccion marker_green"
        esp="2-Delegado de Protección de Datos:"
        eng="2-Data Protection Officer:"
        cat="2-Delegat de Protecció de Dades:"
    ></b>
    <span class="traduccion"
        esp="Asimismo, le informamos de que esta Entidad ha llamado a un responsable de Protección de Datos con quien podrá contactar en la siguiente dirección de correo electrónico"
        eng="We also inform you that this Entity has appointed a Data Protection Officer who can be contacted at the following e-mail address"
        cat="Així mateix, li informem de que aquesta Entitat ha anomenat a un responsable de Protecció de Dades amb qui podrà contactar en la següent direcció de correu electrònic"
    ></span>
    <a href="mailto:contact@studio-liquid.com">contact@studio-liquid.com</a>
</p>
<p>
    <b class="traduccion marker_green"
        esp="3-Finalidad del tratamiento:"
        eng="3-Purpose of treatment:"
        cat="3-Finalitat del tractament:"
    ></b>
    <span class="traduccion"
        esp="La información personal que Studio Liquid posee sobre usted, será utilizada por el desarrollo y cumplimiento de las obligaciones previstas en el presente contrato. Tratando sus datos con el fin de informarle sobre actividades, artículos de interés e información general sobre nuestros productos y servicios."
        eng="The personal information that Studio Liquid holds about you, will be used for the development and fulfillment of the obligations under this contract. Processing your data in order to inform you about activities, articles of interest and general information about our products and services."
        cat="La informació personal que Studio Liquid posseeix sobre vostè, serà utilitzada pel desenvolupament i compliment de les obligacions previstes en el present contracte. Tractant les seves dades amb la finalitat d'informar-lo sobre activitats, articles d'interès i informació general sobre els nostres productes i serveis."
    ></span>
</p>
<p>
    <b class="traduccion marker_green"
        esp="4-Legitimación:"
        eng="4-Legitimation:"
        cat="4-Legitimació:"
    ></b>
    <span class="traduccion"
        esp="La legitimación para el tratamiento de sus datos se basan en:"
        eng="The legitimacy for the processing of your data is based on:"
        cat="La legitimació per el tractament de les seves dades es basen en:"
    ></span>
</p>
<p class="tab">
    <b class="marker_green">4.1-</b>
    <span class="traduccion"
        esp="El contrato suscrito con Studio Liquid."
        eng="The contract with Studio Liquid."
        cat="El contracte subscrit amb Studio Liquid."
    ></span>
</p>
<p class="tab">
    <b class=" marker_green" >4.2-</b>
    <span class="traduccion"
        esp="El consentimiento prestado por usted para el tratamiento de sus datos personales, ya sea por una o diferentes finalidades específicas."
        eng="The consent given by you to the processing of your personal data, whether for one or more specific purposes."
        cat="El consentiment prestat per vostè pel tractament de les seves dades personals, ja sigui per una o diferents finalitats específiques."
    ></span>
</p>
<p>
    <b class="traduccion marker_green"
        esp="5-Destinatario:"
        eng="5-Target:"
        cat="5-Destinatari:"
    ></b>
    <span class="traduccion"
        esp="Sus datos no serán cedidos para otras finalidades diferentes a las anteriormente nombradas."
        eng="Your data will not be disclosed for purposes other than those mentioned above."
        cat="Les seves dades no seran cedides per altres finalitats diferents a les anteriorment anomenades."
    ></span>
</p>
<p>
    <b class="traduccion marker_green"
        esp="6-Derechos:"
        eng="6-Rights:"
        cat="6-Drets:"
    ></b>
    <span class="traduccion"
        esp="Derechos de Acceso, Rectificación y Supresión: Usted tiene derecho a obtener confirmación sobre el tratamiento de sus datos de carácter personal por parte de la entidad. En concreto, usted tiene derecho a acceder a sus datos personales, así como solicitar las rectificaciones de datos inexactos o, en supuesto, solicitar la supresión, entre otros motivos, los datos que no sean necesarias para los fines que fueron recogidas."
        eng="Rights of Access, Rectification and Deletion: You have the right to obtain confirmation about the processing of your personal data by the entity. Specifically, you have the right to access your personal data, as well as to request the rectification of inaccurate data or, of course, request the deletion, among other reasons, of data that are not necessary for the purposes for which they were collected. Translated with www.DeepL.com/Translator (free version)"
        cat="Drets d'Accés, Rectificació i Supressió: Vostè te dret a obtenir confirmació sobre el tractament de les seves dades de caràcter personal per part de l'entitat. En concret, vostè te dret a accedir a les seves dades personals, així com sol·licitar les rectificacions de dades inexactes o, en el suposat, sol·licitar la supressió, entre altres motius, les dades que no siguin necessàries per a les finalitats que varen ser recollides."
    ></span>
</p>
<p class="traduccion"
    esp="Derecho a la Limitación y Oposición: En determinadas circunstancias, usted podrá solicitar la limitación del tratamiento de sus datos, en el supuesto, únicamente las conservaremos por el ejercicio o la defensa de reclamaciones. En determinadas circunstancias y por motivos relacionados con su situación particular, usted podrá oponerse al tratamiento de sus datos. En este caso, la entidad dejará de tratar sus datos, salvo que, por motivos legítimos imperiosos, o por el ejercicio o defensa de posibles reclamaciones."
    eng="Right to Restriction and Opposition: In certain circumstances, you may request that we restrict the processing of your data, in which case we will only retain it for the exercise or defence of claims. In certain circumstances and for reasons relating to your particular situation, you may object to the processing of your data. In this case, the entity will stop processing your data, except for compelling legitimate reasons, or for the exercise or defence of possible claims.
Translated with www.DeepL.com/Translator (free version)"
    cat="Dret a la Limitació i Oposició: En determinades circumstancies, vostè podrà sol·licitar la limitació del tractament de les seves dades, en el suposat, únicament les conservarem per l'exercici o la defensa de reclamacions. En determinades circumstancies i per motius relacionats amb la seva situació particular, vostè podrà oposar-se al tractament de les seves dades. En aquest cas, l'entitat deixarà de tractar les seves dades, llevat que, per motius legítims imperiosos, o per l'exercici o defensa de possibles reclamacions."
></p>
<p>
    <span class="traduccion"
        esp="Para ejercer los derechos anteriormente llamados deberá dirigirse a"
        eng="To exercise the aforementioned rights, you should address your request to"
        cat="Per exercir els drets anteriorment anomenats s'haurà d'adreçar a"
    ></span>
    <a href="mailto:contact@studio-liquid.com">contact@studio-liquid.com</a>
    <span class="traduccion"
        esp=". Le informamos de que la Agencia Española de Protección de Datos es el órgano competente destinado a la tutela de estos derechos."
        eng=". We inform you that the Spanish Data Protection Agency is the competent body for the protection of these rights."
        cat=". L'informem de que l'Agencia Española de Protecció de Dades es l'òrgan competent destinat a la tutela d'aquests drets."
    ></span>
</p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Con el fin de mantener actualizados los datos, el afectado deberá comunicar cualesquiera cambio que se produzca sobre sí mismos."
    eng="In order to keep the data up to date, the data subject shall communicate any changes to his or her data."
    cat="Amb la finalitat de mantenir actualitzades les dades, l'afectat haurà de comunicar qualssevol canvi que es produeixi sobre si mateixos."
></p>
