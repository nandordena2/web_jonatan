<?php
    $DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
    $DOCUMENT_HTTP = "http".(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']!="off"?"s":"")."://".$_SERVER["SERVER_NAME"];
?>
<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/footer.css">
<footer>
    <div class="traduccion contacto"
        esp="Contáctanos"
        eng="Contact with us"
        cat="Contacta’ns"
    ></div>
    <a href="mailto:contact@studio-liquid.com">contact@studio-liquid.com</a>
    <div class="telf">
        <a href="tel:+34 626 88 76 75">+34 626 88 76 75</a>
        <a href="tel:+34 639 08 02 84">+34 639 08 02 84</a>
    </div>
    <div class="rrss">
        <a href="#">
            <img src="<?php echo $DOCUMENT_HTTP?>/img/instagram_liquid.svg" alt="instagram">
        </a>
        <a href="#">
            <img src="<?php echo $DOCUMENT_HTTP?>/img/linkedin_liquid.svg" alt="linkedin">
        </a>
    </div>
    <div class="copy">
        Copyright © 2019, Sudio Liquid all rights reserved
        <a href="/privacidad" class="privacity">| Privacy Policy |</a>
    </div>
</footer>
