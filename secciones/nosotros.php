<link rel="stylesheet" href="<?php echo $DOCUMENT_HTTP?>/secciones/nosotros.css">
<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/liquid-ceo.jpg" alt="ceo" class="imagen_slide">
</div>
<div class="texto">
    <h2 class="traduccion ancho_maximo"
        esp="¿Quienes somos?"
        cat="Qui som?"
        eng="Who are we?"
    ></h2>
    <p class="traduccion ancho_maximo"
        esp="Studio Liquid nace con el fin de dar soporte técnico a las empresas y aportar soluciones reales y competitivas en el diseño y desarrollo de producto, adaptándose a las necesidades, tanto de nuestros clientes como la de los sus consumidores. "
        cat="Studio Liquid neix amb la finalitat de donar suport tècnic a les empreses i aportar solucions reals i competitives en el disseny i desenvolupament de producte, adaptant-nos a les necessitats, tant dels nostres clients com la dels seus consumidors."
        eng="Studio Liquid was created with the aim of giving technical support to companies and providing real and competitive solutions in the design and development of products. product design and development, adapting to the needs of both our clients and their consumers. their consumers. "
    ></p>
    <p class="traduccion ancho_maximo"
        esp="El estudio está integrado por un equipo de diseñadores e ingenieros industriales con una amplia trayectoria dentro del sector industrial, uniendo experiencia y conocimiento para oferior soluciones integrales a nuestros clientes. "
        cat="L’estudi està integrat per un equip de dissenyadors i enginyers industrials amb una àmplia trajectòria dins el sector industrial, unint experiència i coneixement per oferior solucions integrals als nostres clients."
        eng="The studio is made up of a team of industrial designers and engineers with extensive experience in the industrial sector, combining experience and knowledge to offer integral solutions to our clients. industrial sector, combining experience and knowledge to offer integral solutions to our clients. "
    ></p>
    <div class="vertical logos_empresas sin_margen">
        <img src="<?php echo $DOCUMENT_HTTP?>/img/liquid.svg" alt="liquid">
        <span>+</span>
        <img src="<?php echo $DOCUMENT_HTTP?>/img/aude.svg" alt="aude">
    </div>
</div>
<div class="texto gris">
    <h2 class="traduccion ancho_maximo"
        esp="¿Que ofrecemos?"
        cat="Què oferim?"
        eng="What do we offer?"
    ></h2>
    <p class="traduccion ancho_maximo"
        esp="Nuestra metodología de trabajo nos permite hacer un seguimiento del proyecto, conjuntamente con nuestros clientes, compartiendo nuestras experiencias y conocimientos para analizar, identificar y solucionar necesidades, a fin de impulsar nuevas ideas competitivas y diferenciadoras. "
        cat="La nostra metodologia de treball ens permet fer un seguiment del projecte, conjuntament amb els nostres clients, compartint les nostres experiències i coneixements per analitzar, identificar i solucionar necessitats, per tal d'impulsar noves idees competitives i diferenciadores."
        eng="Our work methodology allows us to monitor the project, together with our clients, sharing our experience and knowledge to analyse, identify and solve needs, in order to promote new competitive and differentiating ideas. "
    ></p>
    <div class="vertical ancho_maximo">
        <div class="texto">
            <h3 class="traduccion"
                esp="Suport a la Ingeniería"
                eng="Engineering Support"
                cat="Suport a l’Enginyeria"
            ></h3>
            <p class="traduccion"
                esp="Dedicamos nuestra actividad profesional a apoyar las ingenierías. Implementamos sus ideas, optimizando los procesos y los costes resultantes, adecuándonos a las tecnologías y legislaciones del mercado."
                eng="We dedicate our professional activity to supporting engineering companies. We implement your ideas, optimising the processes and the resulting costs, adapting to the technologies and legislations of the market."
                cat="Dediquem la nostra activitat professional a donar suport a les enginyeries. Implementem les seves idees, optimitzant els processos i els costos resultants, adequant-nos a les tecnologies i legislacions del mercat."
            ></p>
        </div>
        <div class="texto">
            <h3 class="traduccion"
                esp="Diseño + Desarrollo"
                eng="Design + Development "
                cat="Disseny + Desenvolupament"
            ></h3>
            <p class="traduccion"
                esp="Diseñamos y desarrollamos para ofrecerle productos innovadores que aporten valor a su empresa analizando sus necesidades y las de sus clientes."
                eng="We design and develop to offer you innovative products that add value to your company by analysing your needs and those of your customers."
                cat="Dissenyem i desenvolupem per oferir-li productes innovadors que aportin valor a la seva empresa analitzant les seves necessitats i les dels seus clients."
            ></p>
        </div>
    </div>
</div>
<div class="texto vertical logos_empresas ancho_maximo logos_colaboraciones scroll_galeria">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/raypa.png" alt="raypa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/ancar.png" alt="ancar">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/lavakan.png" alt="lavakan">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/balliu.png" alt="balliu">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/verdes.png" alt="verdés">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/vitrosep.png" alt="vitrosep">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/yter.png" alt="yter">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/trinity industries.png" alt="trinity industries">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/espa.png" alt="espa">
</div>
