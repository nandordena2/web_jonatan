<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv_works.png" alt="TLV Series" class="imagen_slide">
</div>
<h3 class="traduccion left"
>TLV Series</h3>
<span class="subtitulo traduccion" >RAYPA</span>
<p class="traduccion"
    esp="Diseño de una gama de autoclaves que solucionan las carencias de los modelos AES / AE-DRY 8-150."
    eng="Design of a range of autoclaves that solve the shortcomings of the AES / AE-DRY 8-150 models."
    cat="Disseny d'una gamma d'autoclaus que solucionen les mancances dels models AES / AE-DRY 8-150."
></p>
<p class="traduccion"
    esp="La serie TLV ofrece unas geometrías limpias y estilizadas, consolidadas para ofrecer una mayor accesibilidad y funcionalidad del equipo, incorporando sistemas automatizados, una pantalla táctil y una interfaz mejorada, para un mayor control."
    eng="The TLV series offers clean, streamlined geometries, consolidated to offer greater accessibility and equipment functionality, incorporating automated systems, a touch screen and an enhanced interface for greater control."
    cat="La sèrie TLV ofereix unes geometries netes i estilitzades, consolidades per oferir una major accessibilitat i funcionalitat de l'equip, incorporant sistemes automatitzats, una pantalla tàctil i una interfície millorada, per a un major control."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="Las autoclaves para laboratorio verticales de RAYPA han sido diseñadas para una amplia gama de aplicaciones y sectores: centros de investigación, universidades, biotecnología, alimentación y farmacéutica. Disponibles 14 modelos de autoclaves verticales con volúmenes de cámaras desde 33 hasta 175 litros."
    eng="RAYPA's vertical laboratory autoclaves are designed for a wide range of applications and sectors: research centres, universities, biotechnology, food and pharmaceutical. 14 models of vertical autoclaves are available with chamber volumes from 33 to 175 litres."
    cat="Les autoclaus per laboratori verticals de RAYPA han estat dissenyades per una amplia gamma d’aplicacions i sectors: centres d’investigació, universitats, biotecnologia, alimentació i farmacèutica. Disponibles 14 models d’autoclaus verticals amb volums de cambres des de 33 fins a 175 litres."
></p>
<div class="img_diapositivas" pos="0" max="6">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (1).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (2).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (3).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (4).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (5).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (6).jpg" alt="TLV Series">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/tlv (7).jpg" alt="TLV Series">
    <div class="selector"></div>
</div>
