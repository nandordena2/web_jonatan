<div class="contenedor-imagen-adaptativa">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer_works.png" alt="effimer" class="imagen_slide">
</div>
<h3 class="traduccion left"
    esp="Línea de productos efímeros"
    eng="Effimer product line "
    cat="Línia de productes efímers"
></h3>
<span class="subtitulo traduccion" >Efﬁmer!</span>
<p class="traduccion"
    esp="Empresa dedicada a la venta y distribución de productos efímeros destinados a eventos y catering. Mantiene la constante necesidad de ofrecer recipientes y bandejas versátiles que permiten adaptarse a las exigencias y creatividad de los profesionales del mundo de la gastronomía."
    eng="Company dedicated to the sale and distribution of ephemeral products for events and catering. It maintains the constant need to offer versatile containers and trays that can be adapted to the demands and creativity of professionals in the world of gastronomy."
    cat="Empresa dedicada a la venda i distribució de productes efímers destinats a esdeveniments i càtering. Manté la constant necessitat d'oferir recipients i safates versàtils que permeten adaptar-se a les exigències i creativitat dels professionals del món de la gastronomia."
></p>
<p class="traduccion margin_ultimo_parrafo"
    esp="La gama de productos desarrollados ofrece recipientes que otorgan valores e ideas reconocibles para los comensales, aportando diseños innovadores y originales, sin comprometer su funcionalidad."
    eng="The range of products developed offers containers that provide recognisable values and ideas for diners, providing innovative and original designs, without compromising on functionality."
    cat="La gamma de productes desenvolupats ofereix recipients que atorguen valors i idees reconeixibles per als comensals, aportant dissenys innovadors i originals, sense comprometre la seva funcionalitat."
></p>
<div class="img_diapositivas" pos="0" max="6">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (7).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (1).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (2).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (3).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (4).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (5).jpg" alt="effimer">
    <img src="<?php echo $DOCUMENT_HTTP?>/img/effimer (6).jpg" alt="effimer">
    <div class="selector"></div>
</div>
